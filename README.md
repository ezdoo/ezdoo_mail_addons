Ezdoo Mail Addons for Odoo
==========================

This basic module to use mailgun api to receive and send emails through Mailgun service.

Available version
----------------
| Addon name                                                    | Version   | Description                                                               |
|--------------------------------------------------------------	|----------	|-----------------------------------------------------------------------	|
| [mailgun-api](/ezdoo/odoo_mailgun-api/mailgun-api/tree/8.0)   | 8.0.1.0   | Base API for receiving and sending emails through the Mailgun service     |
| [mailgun-api](/ezdoo/odoo_mailgun-api/mailgun-api/tree/10.0)  | 10.0.1.0  | Base API for receiving and sending emails through the Mailgun service     |

WARNING
-------

**_This is a alpha version, not really tested as should._**

_Please, use carefully, use module at your own risk_
